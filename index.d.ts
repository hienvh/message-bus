import { ProducerGlobalConfig, ConsumerGlobalConfig, ProducerTopicConfig, ConsumerTopicConfig } from "node-rdkafka";

interface StreamOptions {
    topics: String[] | String | RegExp,
    waitInterval: Number
}

interface ProducerStreamConfig {
    topic: String
}

interface Producer {
    constructor(config: ProducerGlobalConfig, topicConfig: ProducerTopicConfig, streamOptions: ProducerStreamConfig) : Producer,
    publish(message: String) : Boolean
}

interface Consumer {
    constructor(config: ConsumerGlobalConfig, topicConfig: ConsumerTopicConfig) : Consumer,
    subscribe(options: StreamOptions, handler: Function) : void
}