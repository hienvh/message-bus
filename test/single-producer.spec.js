const assert = require('assert')
const SingleProducer = require('../src/SingleProducer')
const config = {
    'client.id': 'mocha-test',
    'metadata.broker.list': 'localhost:9092',
    // 'retry.backoff.ms': 200,
    // 'message.send.max.retries': 10,
    // 'socket.keepalive.enable': true,
    // 'queue.buffering.max.messages': 100000,
    // 'queue.buffering.max.ms': 1000,
    // 'batch.num.messages': 1000000,
    // 'dr_cb': true
}

describe('Single Producer', function () {
    this.timeout(0)
    it('Produce multi message', async () => {
        const producer = new SingleProducer(config)
        await Promise.all([
            producer.produce({
                topic: "mocha-test",
                partition: null,
                message: {test:1},
                keyMessage: null,
                timestamp: Date.now()
            }),
            producer.produce({
                topic: "mocha-test",
                partition: null,
                message: {test:2},
                keyMessage: null,
                timestamp: Date.now()
            }),
            producer.produce({
                topic: "mocha-test",
                partition: null,
                message: {test:3},
                keyMessage: null,
                timestamp: Date.now()
            }),
            producer.produce({
                topic: "mocha-test",
                partition: null,
                message: {test:4},
                keyMessage: null,
                timestamp: Date.now()
            }) 
        ])

        const producer2 = new SingleProducer(config)

        await producer2.produce({
            topic: "mocha-test",
            partition: null,
            message: {test:5},
            keyMessage: null,
            timestamp: Date.now()
        })
    })

    it('After connect', () => {
        const producer = new SingleProducer(config)
        assert.equal(producer.isConnected(), true)
    })

    it('Faster produce after connect', () => {
        const producer = new SingleProducer(config)
        return producer.produce({
            topic: "mocha-test",
            partition: null,
            message: {test:6},
            keyMessage: null,
            timestamp: Date.now()
        }) 
    })

    it('Performance asynchonize', () => {
        for(let i =0; i<100000; i++) {
            const config2 = {
                'client.id': 'mocha-test',
                'metadata.broker.list': 'localhost:9092',
                // 'retry.backoff.ms': 200,
                // 'message.send.max.retries': 10,
                // 'socket.keepalive.enable': true,
                'queue.buffering.max.messages': 100000,
                'queue.buffering.max.ms': 1000,
                'batch.num.messages': 1000000,
                // 'dr_cb': true
            }
            let producer = new SingleProducer(config)
            producer.produce({
                topic: "mocha-test",
                partition: null,
                message: {test:6},
                keyMessage: null,
                timestamp: Date.now()
            }) 
        }
    })

    it('Performance synchonize', async () => {
        for(let i =0; i<10000; i++) {
            const config2 = {
                'client.id': 'mocha-test',
                'metadata.broker.list': 'localhost:9092',
                // 'retry.backoff.ms': 200,
                // 'message.send.max.retries': 10,
                // 'socket.keepalive.enable': true,
                'queue.buffering.max.messages': 100000,
                'queue.buffering.max.ms': 1000,
                'batch.num.messages': 1000000,
                // 'dr_cb': true
            }
            let producer = new SingleProducer(config)
            await producer.produce({
                topic: "mocha-test",
                partition: null,
                message: {test:6},
                keyMessage: null,
                timestamp: Date.now()
            }) 
        }
    })
})
