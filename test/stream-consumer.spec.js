const StreamConsumer = require('../src/StreamConsumer')
const StreamProducer = require('../src/StreamProducer')
const kafkaConfig = {
    'client.id': 'mocha-test',
    'metadata.broker.list': 'localhost:9092',
}

const consumerConfig = {
    'group.id': 'kafka',
    'metadata.broker.list': 'localhost:9092',
}

const topicConfig = {
    'auto.offset.reset': 'earliest'
}

describe('Stream consumer', function() {
    this.timeout(0)

    it('Consume message', function() {
        const producer = new StreamProducer(kafkaConfig, {}, {topic: 'mocha-consumer-test'})
        for(let i=0; i< 5; i++) {
            producer.publish(JSON.stringify('{test:1}'))
        }

        const consumer = new StreamConsumer(consumerConfig, topicConfig)
        const handler = (message) => {
            console.log(message);
        }
        consumer.subscribe({topics: 'mocha-consumer-test'}, handler)
    })


    it('multi consumer', function() {
        const producer = new StreamProducer(kafkaConfig, {}, {topic: 'mocha-consumer-test'})
        for(let i=0; i< 5; i++) {
            producer.publish(JSON.stringify('{test:1}'))
        }

        const producer2 = new StreamProducer(kafkaConfig, {}, {topic: 'mocha-consumer-test2'})
        for(let i=0; i< 5; i++) {
            producer2.publish(JSON.stringify('{verylongkey:"veryveryveryverylongvalue"}'))
        }

        const consumer = new StreamConsumer(consumerConfig, topicConfig)
        const handler = (message) => {
            console.log(message.value.toString());
        }
        consumer.subscribe({topics: 'mocha-consumer-test'}, handler)
        consumer.subscribe({topics: 'mocha-consumer-test2'}, handler)

    })
})