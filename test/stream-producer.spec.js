const StreamProducer = require("../src/StreamProducer")
const assert = require('assert')
const config = {
    'client.id': 'mocha-test',
    'metadata.broker.list': 'localhost:9092',
    // 'retry.backoff.ms': 200,
    // 'message.send.max.retries': 10,
    // 'socket.keepalive.enable': true,
    // 'queue.buffering.max.messages': 100000,
    // 'queue.buffering.max.ms': 1000,
    // 'batch.num.messages': 1000000,
    // 'dr_cb': true
}

const topicConfig = {}
const streamConfig = {
    topic: 'mocha-test'
}

describe('Stream producer', function() {
    this.timeout(0)
    it('Normal', function() {
        const mochaProducer = new StreamProducer(config, topicConfig, streamConfig)
        const queue = mochaProducer.publish(JSON.stringify({test: 1}))
        assert(queue, true)
    })

    it('Many publish', function() {
        const mochaProducer = new StreamProducer(config, topicConfig, streamConfig)
        let result = []
        for(let i = 0; i < 10; i++) {
            result.push(mochaProducer.publish(JSON.stringify({test: 1})))
        }

        assert(result, Array(10).fill(true))
    })

    it('Performance', function() {
        const mochaProducer = new StreamProducer(config, topicConfig, streamConfig)
        let result = []
        for(let i = 0; i < 100000; i++) {
            result.push(mochaProducer.publish(JSON.stringify({test: i+10000})))
        }

        assert(result, Array(100000).fill(true))
    })

    it('Multi producer', function() {
        const mochaProducer = new StreamProducer(config, topicConfig, streamConfig)
        const mochaProducer2 = new StreamProducer(config, topicConfig, streamConfig)
        let result = []
        for(let i = 0; i < 10; i++) {
            result.push(mochaProducer.publish(JSON.stringify({test: 1})))
        }

        for(let i = 0; i < 10; i++) {
            result.push(mochaProducer2.publish(JSON.stringify({test: 1})))
        }

        assert(result, Array(20).fill(true))
    })
})