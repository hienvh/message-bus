const SingleConsumer = require('../src/SingleConsumer')
const SingleProducer = require('../src/SingleProducer')
const producerConfig = {
    'client.id': 'mocha-test',
    'metadata.broker.list': 'localhost:9092',
}

const consumerConfig = {
    'group.id': 'kafka',
    'metadata.broker.list': 'localhost:9092',
}

const topicConfig = {
    'auto.offset.reset': 'earliest'
}

describe('Single consumer', function() {
    this.timeout(0)
    beforeEach(function() {
        const producer = new SingleProducer(producerConfig)
        return producer.produce({
            topic: "mocha-consumer-test",
            partition: null,
            message: {test:1},
            keyMessage: null,
            timestamp: Date.now()
        }) 
    })

    it('Consume message', function() {
        const producer = new SingleProducer(producerConfig)
        for(let i=0; i< 5; i++) {
            producer.produce({
                topic: "mocha-consumer-test",
                partition: null,
                message: {test:1},
                keyMessage: null,
                timestamp: Date.now()
            })
        }

        const consumer = new SingleConsumer(consumerConfig, topicConfig, ['mocha-consumer-test'], function(data) {
            console.log(data.value.toString())
        })
        consumer.consume()
    })
})