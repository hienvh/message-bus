const Kafka = require('node-rdkafka');

class StreamConsumer {
    constructor(kafkaConfig, topicConfig) {
        this.kafkaConfig = kafkaConfig
        this.topicConfig = topicConfig
    }

    subscribe(options, handler) {
        const handlerType = typeof handler
        if(handlerType !== 'function') {

            throw new TypeError(`Handler must be a function, ${handlerType} given`);
        }

        this.stream = Kafka.KafkaConsumer.createReadStream(this.kafkaConfig, this.topicConfig, options)
        this.stream.on('data', handler)
        this.stream.on('error', (err) => {
            console.log(err)
        })

        this.stream.consumer.on('event.error', function(err) {
            console.log(err);
        })
    }
}

module.exports = StreamConsumer