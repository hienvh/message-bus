const Kafka = require('node-rdkafka');

class StreamProducer {
    constructor(kafkaConfig, topicConfig, streamOption) {
        this.kafkaConfig = kafkaConfig
        this.topicConfig = topicConfig
        this.streamOption = streamOption
        this.stream = Kafka.Producer.createWriteStream(kafkaConfig, topicConfig, streamOption)
        this.stream.on('error', this.onError)
    }

    onError(err) {

    }

    publish(message) {
        if(typeof message === 'object') {
            message = JSON.stringify(message)
        }

        const publishMessage = Buffer.from(message)
        const queueMessage = this.stream.write(publishMessage)
        if(queueMessage) {
            return true
        } else {
            return false
        }
    }
}

module.exports = StreamProducer