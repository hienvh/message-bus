const Kafka = require('node-rdkafka');

class SingleProducer {
    constructor(globalConfig, topicConfig) {

        if(!!SingleProducer.instance) {
            return SingleProducer.instance
        }

        this.globalConfig = globalConfig
        this.topicConfig = topicConfig
        
        this.producer = new Kafka.HighLevelProducer(globalConfig, topicConfig)
        this.producer.setValueSerializer(value => {
            return Buffer.from(JSON.stringify(value))
        })

        SingleProducer.instance = this

        return this
    }

    isConnected() {
        return this.producer.isConnected()
    }

    onDelivery (err, offset) {
    }

    async produce ({topic, partition = null, message, keyMessage = null, timestamp = 0}) {
        if(!this.producer.isConnected()) {
            await this.connect()
        }
   
        let exec = new Promise((resolve, reject) => {
            this.producer.produce(topic, partition, message, keyMessage, timestamp, (err, offset) => {
                if(err) reject(err)
                resolve()
            })
            this.producer.poll()
        })

        return await exec
    }

    connectedTime () {
        return this.producer.connectedTime()
    }

    connect() {
        return new Promise((resolve, reject) => {
            this.producer.on('ready', () => {
                resolve();
            })
            this.producer.on('event.error', (err) => {
                reject(err)
            })
            this.producer.on('disconnected', () => {
            })
            this.producer.connect()
        })
    } 
}

module.exports = SingleProducer