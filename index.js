const StreamConsumer = require('./src/StreamConsumer')
const StreamProducer = require('./src/StreamProducer')

module.exports = {
    Consumer: StreamConsumer,
    Producer: StreamProducer
}